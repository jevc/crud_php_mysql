<?php 

    include("inc/conexion.php");
    include("inc/funciones.php");

    if ($_POST["operacion"] == "Crear") {
        $imagen = '';

        if ($_FILES["imagen_usuario"]["name"] != '') {            
            $imagen = subir_imagen();
        }

        $stmt = $conexion->prepare("INSERT INTO usuarios (nombre, apellidos, imagen, telefono, email) VALUES (:nombre, :apellidos, :imagen, :telefono, :email)");

        $resultado = $stmt->execute( array(
            ':nombre'       => $_POST["nombre"],
            ':apellidos'    => $_POST["apellidos"],
            ':telefono'     => $_POST["telefono"],
            ':email'        => $_POST["email"],
            ':imagen'       => $imagen
        ));

        if (!empty($resultado)) {

            enviar_correo('Se creo un nuevo registro. <br/><br/> <strong>Nombre:</strong> ' .$_POST["nombre"] . '<br/> <strong>Apellidos:</strong> ' .$_POST["apellidos"]. '<br/> <strong>Telefono: </strong>' .$_POST["telefono"].'<br/> <strong>Email: </strong>' .$_POST["email"]. '<br/><br/><a href="https://josuevelazquez.mx/projects/crud_php/" target="_blank">Ver proyecto</a>');
            echo 'Registro creado';
        }
    }

    if ($_POST["operacion"] == "Editar") {

        $imagen = '';        

        if ($_FILES["imagen_usuario"]["name"] != '') {            
            $imagen = subir_imagen();
        } else {
            $imagen = $_POST["imagen_usuario_oculto"];
        }        

        $stmt = $conexion->prepare("UPDATE usuarios SET nombre=:nombre, apellidos=:apellidos, imagen=:imagen, telefono=:telefono, email=:email WHERE id = :id");

        $resultado = $stmt->execute( 
            array(
            ':nombre'       => $_POST["nombre"],
            ':apellidos'    => $_POST["apellidos"],
            ':telefono'     => $_POST["telefono"],
            ':email'        => $_POST["email"],
            ':imagen'       => $imagen,
            ':id'           => $_POST["id_usuario"]
            )
        );

        if (!empty($resultado)) {
            echo 'Registro actualizado';
        }
        
    }