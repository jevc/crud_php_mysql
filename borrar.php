<?php 

    include("inc/conexion.php");
    include("inc/funciones.php");

    if (isset($_POST["id_usuario"])) {

        $idUsuario = $_POST["id_usuario"];

        if ($idUsuario != -1) {     // borrado por ID
        
            borrar_datos_usuario($idUsuario, $conexion);
            
        } else {    // borrado masivo por fecha

            $query = "SELECT * FROM usuarios where TIMESTAMPDIFF(MINUTE,fecha_creacion,NOW()) > 30";            

            $stmt = $conexion->prepare($query);
            $stmt->execute();
        
            $resultado = $stmt->fetchAll();
            $datos = array();                    
            $totalRegistrosBorrar =  $stmt->rowCount();

            foreach ($resultado as $fila) {
                borrar_datos_usuario( $fila["id"], $conexion);
            }

            if ($totalRegistrosBorrar > 0)
                enviar_correo('Se invoco la eliminación de ' . $totalRegistrosBorrar . ' registros.');
        }
    } 
    

    function borrar_datos_usuario($id_usuario, $conexion) {

        $imagen = obtener_nombre_imagen($id_usuario);

        if ($imagen != '') {            
            unlink("img/" . $imagen);
        }        

        $stmt = $conexion->prepare("DELETE from usuarios WHERE id = :id");

        $resultado = $stmt->execute( 
            array(            
            ':id'           => $id_usuario
            )
        );

        if (!empty($resultado))
            echo 'Registro borrado';

    }