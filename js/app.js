var dataTable;

$(document).ready(function() {
        
    eliminarRegistrosAntiguos();    

    $("#botonCrear").click( function() {
        
        $("#formulario")[0].reset();
        $(".modal-title").text("Crear usuario");
        $("#action").val("Crear");
        $("#operacion").val("Crear");
        $("#imagen_subido").html("");

    });
   
    $(document).on('submit', '#formulario', function(event){
        agregarInformacion(event, this);        
    });

    $(document).on('click', '.editar', function(event){
        editarInformacion(this);
    });

    $(document).on('click', '.borrar', function(event){
        borrarInformacion(this);
    });
    
});


function agregarInformacion(event, objFrm) {
    
    event.preventDefault();

    var nombre = $("#nombre").val();
    var apellidos = $("#apellidos").val();
    var telefono = $("#telefono").val();
    var email = $("#email").val();

    var extension = $("#imagen_usuario").val().split('.').pop().toLowerCase();

    if (extension != '') {
        if (jQuery.inArray(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert("Formato de imagen inválido");

            $('#imagen_usuario').val('');

            return false;
        }
    }
    
    if ($('#imagen_usuario')[0].files[0] != undefined) {
    
        var file_size = $('#imagen_usuario')[0].files[0].size;
        if(file_size>524288) {
            alert("Solo se aceptan cargar archivos con un peso igual o menor a 500kb.");
            return false;
        } 
    }

    if (nombre != '' && apellidos != '' && email != '') {
        $.ajax({
            url: "crear.php",
            method: "POST",
            data: new FormData(objFrm),
            contentType:false,
            processData:false,
            success: function(data) {
                                
                $("#formulario")[0].reset();
                $("#modalUsuario").modal('hide');

                dataTable.ajax.reload();
                alert(data);
            }
        });
    } else {
        alert("Algunos campos son obligatorios");
    }
}

function borrarInformacion(objBtn) {

    var idUsuario = $(objBtn).attr("id");

    if (confirm("Esta seguro de borrar este registro: " + idUsuario)) {
        
        $.ajax({
            url: "borrar.php",
            method: "POST",
            data:{ id_usuario : idUsuario },            
            success: function(data) {         

                dataTable.ajax.reload();                
                alert(data);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
        
}

function editarInformacion(objBtn) {
        
    var idUsuario = $(objBtn).attr("id");    
    
    $.ajax({
        url: "obtener_registro.php",
        method: "POST",
        data:{id_usuario:idUsuario},
        dataType: "json",        
        success: function(data) {            
                                    
            $("#formulario")[0].reset();                    

            $('#nombre').val(data.nombre);
            $('#apellidos').val(data.apellidos);
            $('#telefono').val(data.telefono);
            $('#email').val(data.email);            
            $('#id_usuario').val(idUsuario);
            $('#imagen_subida').html(data.imagen);
            
            $('.modal-title').text("Editar Usuario");
            $('#action').val("Editar");
            $('#operacion').val("Editar");
            $('#modalUsuario').modal('show');
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });

}

function eliminarRegistrosAntiguos() {

    $.ajax({
        url: "borrar.php",
        method: "POST",
        data:{ id_usuario : -1 },            
        success: function(data) {         

            initDataTable();            

        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });

}

function initDataTable() {
    
    dataTable = $('#datos_usuario').DataTable( {
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax": {
            url:"obtener_registros.php",
            type:"POST"
        },
        "columnsDefs": [{
            "targets":[ 0, 3, 4],
            "orderable": false,
        }]
    });

}
