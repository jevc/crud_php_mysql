<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">

    <title>CRUD PHP + MySQL + Ajax + JQuery + Bootstrap</title>
  </head>
  <body>
    

    <div class="container">
    
        <div class="row text-center">
            <h1>CRUD</h1>
            <h2>PHP + MySQL + Ajax + JQuery + Bootstrap</h2>
        </div>
        
        <div class="row">
            <div class="col-2 offset-10">
                <div class="text-center">
                    <button id="botonCrear" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalUsuario">
                        <i class="bi bi-plus-circle-fill"></i> Crear
                    </button>
                </div>
            </div>
        </div>

        <br/>
        <br/>

        <div class="table-responsive">
            <table id="datos_usuario" class="table table-bordered table-striped">
                <thead>
                    <tr class="text-center">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Teléfono</th>
                        <th>Email</th>
                        <th>Imagen</th>
                        <th>Fecha creación</th>
                        <th>Editar</th>
                        <th>Borrar</th>
                    </tr>
                </thead>
            </table>
        </div>
        <br/>
        <div class="row text-center">            
            <p><a href="https://josuevelazquez.mx/portfolio/crud-php/" target="_self">Información técnica de este proyecto</a> y no olvides visitar mi sitio web <a href="https://josuevelazquez.mx/" target="_blank">Josue Velazquez</a></p>
            <p>Curso gratuito creado por <a href="https://www.udemy.com/user/render2web/" target="_blank">render2web</a> y publicado en <a href="https://www.udemy.com/course/crud-php-pdo-ajax" target="_blank">Udemy</a></p>
            <p>NOTA: Los registros se <strong>eliminarán automáticamente</strong> después de <strong>30 minutos</strong> de su fecha de creación</p>
        </div>

    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="modalUsuario" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ingresar registro</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            
                <form method="POST" id="formulario" enctype="multipart/form-data">
                    <div class="modal-content">

                        <div class="modal-body">
                            <label for="nombre">Ingrese el nombre </label>
                            <input type="text" name="nombre" id="nombre" class="form-control">
                            <br/>
                            <label for="apellidos">Ingrese los apellidos </label>
                            <input type="text" name="apellidos" id="apellidos" class="form-control">
                            <br/>
                            <label for="telefono">Ingrese el teléfono </label>
                            <input type="text" name="telefono" id="telefono" class="form-control">
                            <br/>
                            <label for="email">Ingrese el email </label>
                            <input type="email" name="email" id="email" class="form-control">
                            <br/>
                            <label for="imagen_usuario">Seleccione una imagen </label>
                            <input type="file" name="imagen_usuario" id="imagen_usuario" class="form-control">
                            <span id="imagen_subida"></span>
                            <br/>
                        </div>

                        <div class="modal-footer">
                            
                            <input type="hidden" name="id_usuario" id="id_usuario" />
                            <input type="hidden" name="operacion" id="operacion" />                            
                            <input type="submit" name="action" id="action" class="btn btn-success" value="Crear"></input>

                        </div>
                    </div>
                </form>
                            
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="js/app.js"></script>

  </body>
</html>
