<?php

    function subir_imagen() {

        if (isset($_FILES["imagen_usuario"])) {
            
            $extension = explode('.', $_FILES["imagen_usuario"]['name']);
            $nuevo_nombre = rand() . '.'. $extension[1];
            $ubicacion = './img/' . $nuevo_nombre;
                        
            move_uploaded_file($_FILES["imagen_usuario"]['tmp_name'], $ubicacion);

            return $nuevo_nombre;
        }
    }

    function obtener_nombre_imagen($id_usuario) {
        include('inc/conexion.php');

        $stmt = $conexion->prepare("SELECT imagen FROM usuarios WHERE id = ' $id_usuario '");
        $stmt->execute();

        $resultado = $stmt->fetchAll();

        foreach($resultado as $fila) {
            return $fila["imagen"];
        }
    }

    function obtener_todos_registros() {
        include('inc/conexion.php');

        $stmt = $conexion->prepare("SELECT * FROM usuarios");
        $stmt->execute();

        $resultado = $stmt->fetchAll();

        return $stmt->rowCount();
    }

    function enviar_correo($msg) {

        $to = "contacto@josuevelazquez.mx";
        $subject = "Mensaje desde CRUD PHP";

        $message = "
        <html>
        <head>
        <title>Mensaje desde CRUD PHP</title>
        </head>
        <body>
        <p>".$msg."</p>
        </body>
        </html>
        ";

        // It is mandatory to set the content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers. From is required, rest other headers are optional
        $headers .= 'From: contacto@josuevelazquez.mx' . "\r\n";    

        mail($to,$subject,$message,$headers);

    }
